function tinhTienThue() {
  var hoTen = document.getElementById("ho_ten").value;
  var tongThuNhapNamValue =
    document.getElementById("txt_tong_thu_nhap").value * 1;
  var songuoiphuthuocValue =
    document.getElementById("txt_so_nguoi_phu_thuoc").value * 1;
  var den60tr = 60;
  var den120tr = 120 - den60tr;
  var den210tr = 210 - (den60tr + den120tr);
  var den384tr = 384 - (den60tr + den120tr + den210tr);
  var den624tr = 624 - (den60tr + den120tr + den210tr + den384tr);
  var den960tr = 960 - (den60tr + den120tr + den210tr + den384tr + den624tr);
  var tren960tr =
    tongThuNhapNamValue -
    (den60tr + den120tr + den210tr + den384tr + den624tr + den960tr);

  if (tongThuNhapNamValue > 0 && tongThuNhapNamValue <= 60e6) {
    var thueCaNhan =
      (tongThuNhapNamValue - 4e6 - songuoiphuthuocValue * 1.6e6) * 0.05;
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Họ tên: ${hoTen}, Tiền thu nhập cá nhân: ${thueCaNhan} VNĐ`;
  } else if (tongThuNhapNamValue > 60e6 && tongThuNhapNamValue <= 120e6) {
    var thueCaNhan =
      (tongThuNhapNamValue - 4e6 - songuoiphuthuocValue * 1.6e6) * 0.1;
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Họ tên: ${hoTen}, Tiền thu nhập cá nhân: ${thueCaNhan} VNĐ`;
  } else if (tongThuNhapNamValue > 120e6 && tongThuNhapNamValue <= 210e6) {
    var thueCaNhan =
      (tongThuNhapNamValue - 4e6 - songuoiphuthuocValue * 1.6e6) * 0.15;
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Họ tên: ${hoTen}, Tiền thu nhập cá nhân: ${thueCaNhan} VNĐ`;
  } else if (tongThuNhapNamValue > 210e6 && tongThuNhapNamValue <= 384e6) {
    var thueCaNhan =
      (tongThuNhapNamValue - 4e6 - songuoiphuthuocValue * 1.6e6) * 0.2;
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Họ tên: ${hoTen}, Tiền thu nhập cá nhân: ${thueCaNhan} VNĐ`;
  } else if (tongThuNhapNamValue > 384e6 && tongThuNhapNamValue <= 624e6) {
    var thueCaNhan =
      (tongThuNhapNamValue - 4e6 - songuoiphuthuocValue * 1.6e6) * 0.25;
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Họ tên: ${hoTen}, Tiền thu nhập cá nhân: ${thueCaNhan} VNĐ`;
  } else if (tongThuNhapNamValue > 624e6 && tongThuNhapNamValue <= 960e6) {
    var thueCaNhan =
      (tongThuNhapNamValue - 4e6 - songuoiphuthuocValue * 1.6e6) * 0.3;
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Họ tên: ${hoTen}, Tiền thu nhập cá nhân: ${thueCaNhan} VNĐ`;
  } else {
    var thueCaNhan =
      (tongThuNhapNamValue - 4e6 - songuoiphuthuocValue * 1.6e6) * 0.35;
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Họ tên: ${hoTen}, Tiền thu nhập cá nhân: ${thueCaNhan} VNĐ`;
  }
}
